/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tokenizer;

import java.util.regex.Pattern;

/**
 * Token class. Used for storing token information.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 * @author Originally snipped from http://cogitolearning.co.uk/?p=525 (2015) and
 * modified a little.
 */
public class Token {

    /**
     * TokenInfo contains the regular expression for matching the tokens, also
     * contains the ID's the matching tokens would be assigned.
     */
    public static class TokenInfo {

        public final Pattern regex;
        public final int tokenID;

        public TokenInfo(Pattern regex, int tokenID) {
            super();
            this.regex = regex;
            this.tokenID = tokenID;
        }
    }

    /**
     * A number identifying the given token. This would fairly probably come
     * from the TokenInfo definition.
     */
    private final int tokenID;

    /**
     * A string this token was made from.
     */
    private final String sequence;

    /**
     * Token. Used for storing token sequence and its detected identifier.
     *
     * @param tokenID
     * @param sequence
     */
    public Token(int tokenID, String sequence) {
        this.tokenID = tokenID;
        this.sequence = sequence;
    }

    /**
     * Gets the token ID.
     *
     * @return Returns the token ID.
     */
    public int getTokenID() {
        return this.tokenID;
    }

    /**
     * Gets the string this token was made from.
     *
     * @return sequence
     */
    public String getSequence() {
        return this.sequence;
    }

}
