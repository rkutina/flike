package flike.exceptions;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public  class StackUnderflowException extends Exception {

        public StackUnderflowException() {
            super();
        }

        public StackUnderflowException(String message) {
            super(message);
        }

        public StackUnderflowException(String message, Throwable cause) {
            super(message, cause);
        }

        public StackUnderflowException(Throwable cause) {
            super(cause);
        }
    }