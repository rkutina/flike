package flike.exceptions;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public  class UnknownWordException extends Exception {

        public UnknownWordException() {
            super();
        }

        public UnknownWordException(String message) {
            super(message);
        }

        public UnknownWordException(String message, Throwable cause) {
            super(message, cause);
        }

        public UnknownWordException(Throwable cause) {
            super(cause);
        }
    }