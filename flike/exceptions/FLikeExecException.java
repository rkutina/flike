package flike.exceptions;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class FLikeExecException extends Exception {

    public FLikeExecException() {
        super();
    }

    public FLikeExecException(String message) {
        super(message);
    }

    public FLikeExecException(String message, Throwable cause) {
        super(message, cause);
    }

    public FLikeExecException(Throwable cause) {
        super(cause);
    }
}
