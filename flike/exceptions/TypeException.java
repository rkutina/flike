package flike.exceptions;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public  class TypeException extends Exception {

        public TypeException() {
            super();
        }

        public TypeException(String message) {
            super(message);
        }

        public TypeException(String message, Throwable cause) {
            super(message, cause);
        }

        public TypeException(Throwable cause) {
            super(cause);
        }
    }