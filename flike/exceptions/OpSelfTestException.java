package flike.exceptions;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class OpSelfTestException extends Exception {

        public OpSelfTestException() {
            super();
        }

        public OpSelfTestException(String message) {
            super(message);
        }

        public OpSelfTestException(String message, Throwable cause) {
            super(message, cause);
        }

        public OpSelfTestException(Throwable cause) {
            super(cause);
        }
    }
