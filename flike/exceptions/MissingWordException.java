package flike.exceptions;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class MissingWordException extends Exception {

    public MissingWordException() {
        super();
    }

    public MissingWordException(String message) {
        super(message);
    }

    public MissingWordException(String message, Throwable cause) {
        super(message, cause);
    }

    public MissingWordException(Throwable cause) {
        super(cause);
    }
}
