package flike;

import static flike.HelperFunctions.debug;
import flike.words.Word;
import flike.exceptions.FLikeExecException;
import java.util.HashMap;
import java.util.Stack;

/**
 * This class acts mostly like an external interface for the whole FLike
 * interpreter.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
//TODO: make some variables persistent
//TODO: further code cleanup
//TODO: add arrays, finish Num class sometime
//TODO: possibly add a second data stack...
public class FLikeExec {

    public FLikeExec() {
        this.ec = new ExecContainer();
    }

    public FLikeExec(HashMap<String, Word[]> globwords, HashMap<String, Object> globvars) {
        this.ec = new ExecContainer();

        if (globvars != null) {
            this.ec.globvars = globvars;
        }
        
        if (globwords != null) {
            this.ec.globwords = globwords;
        }

    }

    /**
     * Contains all the variables needed for actual FLikeExec execution.
     */
    private final ExecContainer ec;

    /**
     * A helper function returning 'nicely formatted' error messages.
     *
     * @param message A message to append to the end of the error.
     * @param code An array of the currently executed line tokens.
     * @param ptr A pointer to the point where the error happened. It's just an
     * index of the array above.
     * @return A nicely formatted string.
     */
    private String getErrorString(String message, Word[] code, int ptr) {
        String ret = "";
        int spacecounter = 0;
        int errlen = 1;
        int toDisplay = 19;

        //TODO: make it always display the set amount of stuff, not just a half of it...
        if (Math.max(0, ptr - toDisplay / 2) != 0) {
            ret = "... ";
        }

        ptr--;

        for (int i = Math.max(0, ptr - toDisplay / 2); (i < code.length) && (i <= (ptr + toDisplay / 2)); i++) {
            ret += code[i].toString() + " ";

            if (i < ptr) {
                spacecounter = ret.length();
            }
            if (i == ptr) {
                errlen = code[i].toString().length();
            }
        }

        if ((ptr + toDisplay / 2) < code.length) {
            ret += "...";
        }

        ret += "\n";
        for (int i = 0; i < spacecounter; i++) {
            ret += " ";
        }

        for (int i = 0; i < errlen; i++) {
            ret += "^";
        }

        return message + "\n" + ret;
    }

    public ExecContainer exec(Word[] compiled, Stack<Object> dStack_in) throws FLikeExecException, Exception {

        //TODO: make some variables persistent
        if (dStack_in != null) {
            ec.dStack = dStack_in;
        }

        System.out.println("");

        debug("\n--- Starting execution ---");
        
        ec.clear();
        ec.program.push(compiled);
        ec.rStack.push(0);
        
        while (!ec.program.empty()){
            ec.ptr = ec.rStack.pop();
            while (ec.ptr < ec.program.peek().length) {
                Word[] cur = ec.program.peek();
                debug("Exec: " + ec.ptr + " - " + cur[ec.ptr].toString());
                cur[ec.ptr].exec(ec);
            }
            ec.program.pop();
        }
        debug("\n--- Finished executing ---");

        return ec;
    }
}
