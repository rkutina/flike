package flike.tokenizer;

import tokenizer.GenericTokenizer;

/**
 * FLikeTokenizer is used for splitting a given FLike code string into tokens.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class FLikeTokenizer extends GenericTokenizer {

    /**
     * Contains token type definitions and patterns to find them.
     */
    public enum TokenType {

        LONGLITERAL(1, "-?[0-9]+"),
        BITLONGLITERAL(2, "(?i)[0-9]+'[bohd]?[0-9a-f]+"),
        FLOATLITERAL(3, "[-+]?[0-9]*\\.?[0-9]+"),
        STRINGLITERAL(4, "\"[^\"]+\""),
        VARALLOC(10, "(?i)var"),
        VAROPS(11, "[-+]?[!@]{1,2}"),
        IF(20, "(?i)if"),
        ELSE(21, "(?i)(else)"),
        THEN(22, "(?i)(then)"),
        DO(30, "(?i)do"),
        LOOP(31, "(?i)loop"),
        BEGIN(32, "(?i)begin"),
        UNTIL(33, "(?i)until"),
        FOREVER(34, "(?i)forever"),
        LEAVE(35, "(?i)leave"),
        WORDDEF_BEGIN(50, ":"),
        WORDDEF_END(51, ";"),
        WORDDEF_BEGIN_GLOB(55, "::|g:|:g"),
        WORDDEF_END_GLOB(56, ";;|g;|;g"),
        GLOBAL(88, "(?i)glob(al)?"),
        IDENTIFIER(99, "[^\\s]+"),
        NULL(0, "");

        private final int tokenID;
        private final String regex;

        TokenType(int i, String regex) {
            this.tokenID = i;
            this.regex = regex;
        }

        public int getTokenID() {
            return this.tokenID;
        }

        public String getRegex() {
            return this.regex;
        }
    };

    public FLikeTokenizer() {
        this.add(TokenType.LONGLITERAL);        //done
        this.add(TokenType.BITLONGLITERAL);     //done
        this.add(TokenType.FLOATLITERAL);       //done
        this.add(TokenType.STRINGLITERAL);      //done
        this.add(TokenType.VARALLOC);           //done
        this.add(TokenType.VAROPS);             //done
        this.add(TokenType.IF);                 //done
        this.add(TokenType.THEN);               //done
        this.add(TokenType.ELSE);               //done
        this.add(TokenType.DO);                 //done
        this.add(TokenType.LOOP);               //done
        this.add(TokenType.BEGIN);              //done
        this.add(TokenType.UNTIL);              //done
        this.add(TokenType.LEAVE);              //done
        this.add(TokenType.FOREVER);            //done
        this.add(TokenType.WORDDEF_BEGIN);      //done
        this.add(TokenType.WORDDEF_BEGIN_GLOB); //done
        this.add(TokenType.WORDDEF_END);        //done
        this.add(TokenType.WORDDEF_END_GLOB);   //done
        this.add(TokenType.GLOBAL);             //done
        
        this.add(TokenType.IDENTIFIER);         //done

    }

    /**
     * Adds a given TokenType definition to the token info LinkedList.
     *
     * @param tt A token type definition to add to the list.
     */
    private void add(TokenType tt) {
        this.addTokenInfo(tt.getRegex() + "\\s+", tt.getTokenID());
    }

    /**
     * Tokenizes the given string. Also strips comments beforehand.
     *
     * @param s A string to tokenize.
     * @throws Exception
     */
    @Override
    public void tokenize(String s) throws Exception {
        super.tokenize(s.replaceAll("\\(\\s+.\\)", ""));
    }

}
