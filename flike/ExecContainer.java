package flike;

import flike.words.Word;
import java.util.HashMap;
import java.util.Stack;

/**
 * This class contains all the variables required for FLike execution. Contains
 * namely the stacks, the instructions to execute, variable storage etc.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class ExecContainer {

    /**
     * Used as a pointer to the currently executed/read word.
     */
    public int ptr;

    /**
     * Data stack. Used to store data to perform operations on or to return.
     */
    public Stack<Object> dStack;
    
    /**
     * Secondary data stack. Fairly handy, I suppose.
     */
    public Stack<Object> sdStack;

    /**
     * Call return stack. Used for storing call return addresses.
     */
    public Stack<Integer> rStack;

    /**
     * Iterator stack.
     */
    public Stack<Long> iStack;
    
    /**
     * HashMap containing all the global word definitions in a compiled form.
     */
    public HashMap<String, Word[]> globwords;
    
    /**
     * The program to execute.
     */
    public Stack<Word[]> program;
    
    /**
     * HashMap of variables. Contains all local user-defined variables.
     */
    public HashMap<String, Object> vars;

    /**
     * HashMap of global variables. Contains all global variables.
     */
    public HashMap<String, Object> globvars;
    
    public ExecContainer() {
        this.ptr = 0;
        this.vars = new HashMap<>();
        this.globvars = new HashMap<>();
        this.globwords = new HashMap<>();
        this.dStack = new Stack<>();
        this.rStack = new Stack<>();
        this.iStack = new Stack<>();
        this.program = new Stack<>();
    }

    public void clear() {
        this.ptr = 0;
        this.vars = new HashMap<>();
        this.dStack = new Stack<>();
        this.sdStack = new Stack<>();
        this.rStack = new Stack<>();
        this.program = new Stack<>();
    }
    
    public void clearGlob(){
        this.clear();
        this.globvars = new HashMap<>();
        this.globwords = new HashMap<>();
    }

}
