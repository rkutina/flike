package flike.words.primitive;

import flike.ExecContainer;
import static flike.HelperFunctions.assertStackDepth;
import flike.exceptions.StackUnderflowException;
import flike.words.PrimitiveWord;

/**
 * Puts the last iterator's value on top of the stack.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class PW_I extends PrimitiveWord {

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException {
        assertStackDepth(ec.iStack, 2);
        ec.dStack.push(ec.iStack.get(ec.iStack.size()-2));

        ec.ptr++;
    }

    @Override
    public String toString() {
        return "PW I";
    }
}
