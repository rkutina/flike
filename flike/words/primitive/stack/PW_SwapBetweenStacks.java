package flike.words.primitive.stack;

import flike.ExecContainer;
import static flike.HelperFunctions.assertStackDepth;
import flike.exceptions.StackUnderflowException;
import flike.words.PrimitiveWord;

/**
 * A primitive word swapping the top values of dStack and sdStack over.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class PW_SwapBetweenStacks extends PrimitiveWord {

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException {
        assertStackDepth(ec.dStack, 1);
        assertStackDepth(ec.sdStack, 1);
        
        Object o1 = ec.dStack.pop();
        Object o2 = ec.sdStack.pop();
        
        ec.sdStack.push(o1);
        ec.dStack.push(o2);

        ec.ptr++;
    }

    @Override
    public String toString() {
        return "PW SwapBetweenStacks";
    }
}
