package flike.words.primitive.stack;

import flike.ExecContainer;
import static flike.HelperFunctions.assertStackDepth;
import flike.exceptions.StackUnderflowException;
import flike.words.PrimitiveWord;

/**
 * PW_Drop. A primitive word defining the operation of the drop operator.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class PW_Drop extends PrimitiveWord {

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException {
        assertStackDepth(ec.dStack, 1);
        
        ec.dStack.pop();

        ec.ptr++;
    }

    @Override
    public String toString() {
        return "PW Drop";
    }
}
