package flike.words.primitive.stack;

import flike.ExecContainer;
import static flike.HelperFunctions.assertStackDepth;
import flike.exceptions.StackUnderflowException;
import flike.words.PrimitiveWord;

/**
 * A primitive word moving the top value of sdStack to dStack.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class PW_MoveOverRev extends PrimitiveWord {

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException {
        assertStackDepth(ec.dStack, 1);
        
        ec.dStack.push(ec.sdStack.pop());

        ec.ptr++;
    }

    @Override
    public String toString() {
        return "PW MoveOver";
    }
}
