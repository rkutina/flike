package flike.words.primitive.stack;

import flike.ExecContainer;
import static flike.HelperFunctions.assertStackDepth;
import flike.exceptions.StackUnderflowException;
import flike.exceptions.TypeException;
import flike.words.PrimitiveWord;

/**
 * PW_DUP. A primitive word defining the operation of the dup operator.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class PW_Dup extends PrimitiveWord {

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException, TypeException {
        assertStackDepth(ec.dStack, 1);

        Object o1 = ec.dStack.pop();
        ec.dStack.push(o1);
        ec.dStack.push(o1); //ugh. gotta figure out how to clone an Object
        
        ec.ptr++;
    }

    @Override
    public String toString() {
        return "PW Dup";
    }
}
