package flike.words.primitive.stack;

import flike.ExecContainer;
import static flike.HelperFunctions.assertStackDepth;
import flike.exceptions.StackUnderflowException;
import flike.exceptions.TypeException;
import flike.words.PrimitiveWord;
import java.util.Stack;

/**
 * A primitive word swapping the two data stacks over.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class PW_SwitchStacks extends PrimitiveWord {

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException, TypeException {
        assertStackDepth(ec.dStack, 2);

        Stack s1 = ec.dStack;
        Stack s2 = ec.sdStack;
        ec.sdStack = s1;
        ec.dStack = s2;
        
        ec.ptr++;
    }

    @Override
    public String toString() {
        return "PW SwapSracks";
    }
}
