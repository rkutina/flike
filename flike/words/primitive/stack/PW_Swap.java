package flike.words.primitive.stack;

import flike.ExecContainer;
import static flike.HelperFunctions.assertStackDepth;
import flike.exceptions.StackUnderflowException;
import flike.exceptions.TypeException;
import flike.words.PrimitiveWord;

/**
 * A primitive word swapping the two topmost values of the dStack over.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class PW_Swap extends PrimitiveWord {

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException, TypeException {
        assertStackDepth(ec.dStack, 2);

        Object o2 = ec.dStack.pop();
        Object o1 = ec.dStack.pop();
        ec.dStack.push(o2);
        ec.dStack.push(o1);
        
        ec.ptr++;
    }

    @Override
    public String toString() {
        return "PW Swap";
    }
}
