package flike.words.primitive.stack;

import flike.ExecContainer;
import static flike.HelperFunctions.assertStackDepth;
import flike.exceptions.StackUnderflowException;
import flike.exceptions.TypeException;
import flike.words.PrimitiveWord;

/**
 * A primitive word bringing the second topmost value's copy on top of the stack.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class PW_Over extends PrimitiveWord {

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException, TypeException {
        assertStackDepth(ec.dStack, 2);

        Object o2 = ec.dStack.pop();
        Object o1 = ec.dStack.pop();
        ec.dStack.push(o1);
        ec.dStack.push(o2);       
        ec.dStack.push(o1); //ugh. gotta figure out how to clone an Object
        
        ec.ptr++;
    }

    @Override
    public String toString() {
        return "PW Over";
    }
}
