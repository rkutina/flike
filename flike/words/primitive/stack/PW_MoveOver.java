package flike.words.primitive.stack;

import flike.ExecContainer;
import static flike.HelperFunctions.assertStackDepth;
import flike.exceptions.StackUnderflowException;
import flike.words.PrimitiveWord;

/**
 * A primitive word moving the top value of dStack to sdStack.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class PW_MoveOver extends PrimitiveWord {

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException {
        assertStackDepth(ec.dStack, 1);
        
        ec.sdStack.push(ec.dStack.pop());

        ec.ptr++;
    }

    @Override
    public String toString() {
        return "PW MoveOver";
    }
}
