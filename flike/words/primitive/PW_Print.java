package flike.words.primitive;

import flike.ExecContainer;
import static flike.HelperFunctions.assertStackDepth;
import flike.exceptions.StackUnderflowException;
import flike.words.PrimitiveWord;

/**
 * Prints the dStacks topmost value and consumes it.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class PW_Print extends PrimitiveWord {

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException {
        assertStackDepth(ec.dStack, 1);
        System.out.print(ec.dStack.pop().toString() + " ");

        ec.ptr++;
    }

    @Override
    public String toString() {
        return "PW print";
    }
}
