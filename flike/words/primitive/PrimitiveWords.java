package flike.words.primitive;

import flike.ExecContainer;
import flike.exceptions.FLikeExecException;
import flike.exceptions.MissingWordException;
import flike.exceptions.OutOfBoundsException;
import flike.exceptions.StackUnderflowException;
import flike.exceptions.TypeException;
import flike.words.primitive.cmp.*;
import flike.words.primitive.math.*;
import flike.words.primitive.stack.*;
import flike.words.primitive.bitwise.*;
import flike.words.PrimitiveWord;
import java.util.HashMap;

/**
 * This class defines a HashMap full of primitive words. Used during the
 * compilation to bind operators to primitive words.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class PrimitiveWords {

    private final HashMap<String, PrimitiveWord> pwords;

    public PrimitiveWords() {
        this.pwords = new HashMap<>();

        //basic 'IO'
        pwords.put(".", new PW_Print());
        pwords.put(".cr", new PrimitiveWord() {

            @Override
            public void exec(ExecContainer ec) throws StackUnderflowException, FLikeExecException, MissingWordException, OutOfBoundsException, TypeException {
                System.out.println("");
                ec.ptr++;
            }

            @Override
            public String toString() {
                return ".CR";
            }
        });
        
        pwords.put("dbg", new PW_DebugDump());
        
        //basic math operators
        pwords.put("+", new PW_Add());
        pwords.put("-", new PW_Sub());
        pwords.put("*", new PW_Mul());
        pwords.put("/", new PW_Div());
        
        //iterator operators
        pwords.put("i", new PW_I());
        
        //basic stack operators
        pwords.put("over", new PW_Over());
        pwords.put("dup", new PW_Dup());
        pwords.put("drop", new PW_Drop());
        pwords.put("swap", new PW_Swap());
        pwords.put("depth", new PW_Depth());
        
        //more stack operators
        pwords.put("sts", new PW_SwitchStacks());
        pwords.put("d>s", new PW_MoveOver());
        pwords.put("d<s", new PW_MoveOverRev());
        pwords.put("d~s", new PW_SwapBetweenStacks());
        
        
        //basic conditions
        pwords.put(">", new PW_Gt());
        pwords.put(">=", new PW_Gte());
        pwords.put("<", new PW_Lt());
        pwords.put("<=", new PW_Lt());
        pwords.put("=", new PW_Eq());
        pwords.put("<>", new PW_Neq());     
        
        //bitwise operators
        pwords.put("&", new PW_And());
        pwords.put("|", new PW_Or());
        pwords.put("^", new PW_Xor());
        pwords.put("~", new PW_Not());
        pwords.put(">>", new PW_ShR());
        pwords.put("<<", new PW_ShL());
    }

    public HashMap<String, PrimitiveWord> getWords() {
        return this.pwords;
    }
}
