package flike.words.primitive;

import flike.ExecContainer;
import static flike.HelperFunctions.debugDump;
import flike.exceptions.StackUnderflowException;
import flike.words.PrimitiveWord;

/**
 * Pushes the dStack's depth (as in size in elements) on top of the dStack.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class PW_DebugDump extends PrimitiveWord {

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException {
        debugDump(ec);
        
        ec.ptr++;
    }

    @Override
    public String toString() {
        return "PW debug dump";
    }
}
