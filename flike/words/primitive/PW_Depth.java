package flike.words.primitive;

import flike.ExecContainer;
import flike.exceptions.StackUnderflowException;
import flike.words.PrimitiveWord;

/**
 * Pushes the dStack's depth (as in size in elements) on top of the dStack.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class PW_Depth extends PrimitiveWord {

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException {
        
        ec.dStack.push((long) ec.dStack.size());
        
        ec.ptr++;
    }

    @Override
    public String toString() {
        return "PW depth";
    }
}
