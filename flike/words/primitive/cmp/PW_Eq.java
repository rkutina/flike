package flike.words.primitive.cmp;

import flike.ExecContainer;
import static flike.HelperFunctions.assertStackDepth;
import flike.exceptions.StackUnderflowException;
import flike.exceptions.TypeException;
import flike.words.PrimitiveWord;
import java.util.Objects;

/**
 * Consumes and compares the two tompost values on the dStack, and puts a 1 on if they match, 0 otherwise.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class PW_Eq extends PrimitiveWord {

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException, TypeException {
        assertStackDepth(ec.dStack, 2);

        Object op2 = ec.dStack.pop();
        Object op1 = ec.dStack.pop();

        //if (op1 instanceof Double && op2 instanceof Double) {
        //    ec.dStack.push( ((Double) op1 == (Double) op2) ? 1L : 0L ); //this one's completely wrong
        /*} else*/ if (op1 instanceof Long && op2 instanceof Long) {
            ec.dStack.push( (Objects.equals((Long) op1, (Long) op2)) ? 1L : 0L );
        } else {
            throw new TypeException("eq: Expected long, got " + op1.getClass().getSimpleName() + " and " + op2.getClass().getSimpleName());
        }

        ec.ptr++;
    }

    @Override
    public String toString() {
        return "PW Eq";
    }
}
