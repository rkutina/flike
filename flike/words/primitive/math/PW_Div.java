package flike.words.primitive.math;

import flike.ExecContainer;
import static flike.HelperFunctions.assertStackDepth;
import flike.exceptions.StackUnderflowException;
import flike.exceptions.TypeException;
import flike.words.PrimitiveWord;

/**
 * Divides the second topmost value on dStack by the topmost one.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class PW_Div extends PrimitiveWord {

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException, TypeException {
        assertStackDepth(ec.dStack, 2);

        Object op2 = ec.dStack.pop();
        Object op1 = ec.dStack.pop();

        if (op1 instanceof Double) {
            if (op2 instanceof Double || op2 instanceof Long) {
                ec.dStack.push((Double) op1 / (Double) op2);
            } else {
                throw new TypeException("* 2: Expected double or long, got " + op2.getClass().getSimpleName());
            }
        } else if (op1 instanceof Long) {
            if (op2 instanceof Long) {
                ec.dStack.push((double) op1 / (double) op2);
            } else if (op2 instanceof Double) {
                ec.dStack.push((double)((Long) op1).doubleValue() / (double) op2);
            } else {
                throw new TypeException("* 2: Expected double or long, got " + op2.getClass().getSimpleName());
            }
        } else {
            throw new TypeException("* 1: Expected double or long, got " + op1.getClass().getSimpleName());
        }

        ec.ptr++;
    }

    @Override
    public String toString() {
        return "PW Div";
    }
}
