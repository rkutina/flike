package flike.words.primitive.bitwise;

import flike.ExecContainer;
import static flike.HelperFunctions.assertStackDepth;
import flike.exceptions.StackUnderflowException;
import flike.exceptions.TypeException;
import flike.words.PrimitiveWord;

/**
 * Logically ORs the two topmost dStack values together.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class PW_Or extends PrimitiveWord {

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException, TypeException {
        assertStackDepth(ec.dStack, 2);

        Object op2 = ec.dStack.pop();
        Object op1 = ec.dStack.pop();

        if (op1 instanceof Long && op2 instanceof Long) {
            ec.dStack.push((long)op1 | (long)op2);
        } else {
            throw new TypeException("or : Expected Long, Long, got "
                    + op1.getClass().getSimpleName() + ", " + op2.getClass().getSimpleName());
        }

        ec.ptr++;
    }

    @Override
    public String toString() {
        return "PW Or";
    }
}
