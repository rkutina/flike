package flike.words.primitive.bitwise;

import flike.ExecContainer;
import static flike.HelperFunctions.assertStackDepth;
import flike.exceptions.StackUnderflowException;
import flike.exceptions.TypeException;
import flike.words.PrimitiveWord;

/**
 * Logically inverts the topmost value of dStack.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class PW_Not extends PrimitiveWord {

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException, TypeException {
        assertStackDepth(ec.dStack, 1);

        Object op1 = ec.dStack.pop();

        if (op1 instanceof Long) {
            ec.dStack.push(~(long)op1);
        } else {
            throw new TypeException("not : Expected Long, got " + op1.getClass().getSimpleName());
        }

        ec.ptr++;
    }

    @Override
    public String toString() {
        return "PW Not";
    }
}
