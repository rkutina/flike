package flike.words.primitive.bitwise;

import flike.ExecContainer;
import static flike.HelperFunctions.assertStackDepth;
import flike.exceptions.StackUnderflowException;
import flike.exceptions.TypeException;
import flike.words.PrimitiveWord;

/**
 * Bit selection. I have yet to figure out how to properly describe the function of this on line line...
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class PW_BitSel extends PrimitiveWord {

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException, TypeException {
        assertStackDepth(ec.dStack, 3);

        //bitsel [thing to bitsel] [bitpos from] [bitpos to]
        Object op3 = ec.dStack.pop();   //to
        Object op2 = ec.dStack.pop();   //from
        Object op1 = ec.dStack.pop();   //thing

        if (op1 instanceof Long && op2 instanceof Long && op3 instanceof Long) {
            long result = (long) op1;
            long from = (long) op2;
            long to = (long) op3;

            long m = Math.max(from, to);
            long n = Math.min(from, to);

            //...____.......
            //   T  F
            //10101110110000 
            //   0111
            
            result = (result & ((~0L) >>> (63 - m))) >>> n;

            if (to <= from) {

            } else {
                //   flip!
                //   1110

                long flipped = 0;

                for (long i = 0; i < (from - to); i++) {
                    System.out.println(i);
                    flipped |= ((result >> i) & 1) << (from - to - 1) - i;
                }

                result = flipped;
            }

            ec.dStack.push(result);
        } else {
            throw new TypeException("shl : Expected Long, Long, Long, got "
                    + op1.getClass().getSimpleName()
                    + ", " + op2.getClass().getSimpleName()
                    + ", " + op3.getClass().getSimpleName());
        }

        ec.ptr++;
    }

    @Override
    public String toString() {
        return "PW ShL";
    }
}
