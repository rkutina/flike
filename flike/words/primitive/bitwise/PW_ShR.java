package flike.words.primitive.bitwise;

import flike.ExecContainer;
import static flike.HelperFunctions.assertStackDepth;
import flike.exceptions.StackUnderflowException;
import flike.exceptions.TypeException;
import flike.words.PrimitiveWord;

/**
 * Shifts the second topmost dStack value right by a number specified in the topmost dStack value.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class PW_ShR extends PrimitiveWord {

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException, TypeException {
        assertStackDepth(ec.dStack, 2);

        Object op2 = ec.dStack.pop();
        Object op1 = ec.dStack.pop();

        if (op1 instanceof Long && op2 instanceof Long) {
            ec.dStack.push((long)op1 >> (long)op2);
        } else {
            throw new TypeException("shr : Expected Long, Long, got "
                    + op1.getClass().getSimpleName() + ", " + op2.getClass().getSimpleName());
        }

        ec.ptr++;
    }

    @Override
    public String toString() {
        return "PW ShR";
    }
}
