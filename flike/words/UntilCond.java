package flike.words;

import flike.ExecContainer;
import static flike.HelperFunctions.assertStackDepth;
import flike.exceptions.StackUnderflowException;
import flike.exceptions.TypeException;

/**
 * A conditional thing used in the Begin-Until loops.
 * 
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class UntilCond extends PrimitiveWord {

    private final int jadr;
    
    public UntilCond(int jadr){
        this.jadr = jadr;
    }
    
    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException, TypeException {
        assertStackDepth(ec.dStack, 1);
        
        Object pop = ec.dStack.pop();
        
        if(pop instanceof Long){
            if( (Long) pop != 0 ){
                ec.ptr = jadr;
            }else{
                ec.ptr++;
            }
        }else{
            throw new TypeException("Cond.Loop: expected Long, got " + pop.getClass().getCanonicalName());
        }
    }

    @Override
    public String toString() {
        return "PW CondJump " + jadr;
    }
    
}
