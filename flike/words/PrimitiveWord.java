package flike.words;

import flike.ExecContainer;
import flike.exceptions.FLikeExecException;
import flike.exceptions.MissingWordException;
import flike.exceptions.OutOfBoundsException;
import flike.exceptions.StackUnderflowException;
import flike.exceptions.TypeException;

/**
 * PrimitiveWord. Used only as an interface for actual primitive words, which
 * are defined in the .primitive package at the point of this writing.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public abstract class PrimitiveWord extends Word {

    @Override
    public abstract void exec(ExecContainer ec)
            throws StackUnderflowException, FLikeExecException, MissingWordException, OutOfBoundsException, TypeException;

    /*
     @Override
     public String toString() {
     return "PRIMITIVE";
     }
     */
}
