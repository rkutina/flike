package flike.words;

import flike.ExecContainer;
import static flike.HelperFunctions.debug;
import flike.exceptions.FLikeExecException;
import java.util.HashMap;

/**
 * VarAllocWord. Used for 'on the fly' variable 'allocation.'
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class VarAllocWord extends Word {

    private final String varname;
    private final Location loc;

    public enum Location {
        GLOBAL, LOCAL;
    }

    public VarAllocWord(String varname, Location loc) {
        this.varname = varname.toLowerCase();
        this.loc = loc;
    }

    @Override
    public void exec(ExecContainer ec) throws FLikeExecException {
        HashMap<String, Object> varstor;
        
        switch(this.loc){
            case GLOBAL:
                debug("VARALLOC: Will try to declare a new global var \'" + varname + "\'");
                varstor = ec.globvars;
                break;
            case LOCAL:
                debug("VARALLOC: Will try to declare a new local var \'" + varname + "\'");
                varstor = ec.vars;
                break;
            default:
                throw new FLikeExecException("VARALLOC: Unknown location");
        }
        
        if(!varstor.containsKey(varname)){
            debug("VARALLOC: If I fits, I sits.");
            varstor.put(varname, 0L);
        }else{
            throw new FLikeExecException("VARALLOC: Trying to redeclare a " +
                    ((loc == Location.GLOBAL) ? "GLOBAL" : "") + " variable \'" + varname + "\'");
        }
        ec.ptr++;
    }

    @Override
    public String toString() {
        return "VARALLOC \'" + varname + "\' " + loc.name();
    }

}
