package flike.words;

import flike.ExecContainer;
import static flike.HelperFunctions.assertKeyExists;
import flike.exceptions.UnknownWordException;
import java.util.HashMap;

/**
 * IncDecVarWord. Defines the behavior of the variable increment/decrement operator.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class IncDecVarWord extends Word {

    private final String identifier;
    private final boolean isGlobal;
    private final boolean incDec;

    public IncDecVarWord(String identifier, boolean isGlobal, boolean incDec) {
        this.identifier = identifier;
        this.isGlobal = isGlobal;
        this.incDec = incDec;
    }

    @Override
    public void exec(ExecContainer ec) throws UnknownWordException {
        HashMap<String, Object> varcontainer = (isGlobal) ? ec.globvars : ec.vars;

        assertKeyExists(varcontainer, this.identifier);

        Object ret = varcontainer.get(identifier);
        if(ret instanceof Long){
            ret = ((Long) ret) + (incDec ? 1 : -1);
        }
        if(ret instanceof Double){
            ret = ((Double) ret) + (incDec ? 1.0 : -1.0);
        }
        
        varcontainer.put(identifier, ret);

        ec.ptr++;
    }

    @Override
    public String toString() {
        return "PW Fetch " + ((isGlobal) ? "G" : "L") + "\'" + identifier + "\'";
    }

}
