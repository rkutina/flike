package flike.words;

import flike.ExecContainer;
import flike.exceptions.MissingWordException;

/**
 * CallWord. Used for jumping to a given address, and returning back to
 * execution beyond this point later.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class CallGlobal extends Word {

    private final String identifier;
    
    public CallGlobal(String identifier) {
        this.identifier = identifier;
    }

    @Override
    public void exec(ExecContainer ec) throws MissingWordException {
        ec.rStack.push(ec.ptr+1);
        ec.ptr = 0;
        if(ec.globwords.containsKey(identifier)){
            ec.program.push(ec.globwords.get(identifier));
        }else{
            throw new MissingWordException("GLOB: cannot find a global word called \'" + identifier + "\'");
        }
    }

    @Override
    public String toString() {
        return "GLOB " + identifier;
    }

}
