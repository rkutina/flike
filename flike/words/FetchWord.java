package flike.words;

import flike.ExecContainer;
import static flike.HelperFunctions.assertKeyExists;
import flike.exceptions.UnknownWordException;
import java.util.HashMap;

/**
 * FetchWord. Defines the behavior of the fetch operator.
 * 
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class FetchWord extends Word {

    private final String identifier;
    private final boolean isGlobal;
    
    public FetchWord(String identifier, boolean isGlobal){
        this.identifier = identifier;
        this.isGlobal = isGlobal;
    }
    
    @Override
    public void exec(ExecContainer ec) throws UnknownWordException {
        HashMap<String, Object> varcontainer = (isGlobal) ? ec.globvars : ec.vars;
        
        assertKeyExists(varcontainer, this.identifier);
        
        ec.dStack.push(varcontainer.get(identifier));
        
        ec.ptr++;
    }

    @Override
    public String toString() {
        return "PW Fetch " + ((isGlobal)?"G":"L") +  "\'" + identifier + "\'";
    }
    
}
