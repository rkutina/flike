package flike.words.placeholders;

import flike.ExecContainer;
import flike.exceptions.MissingWordException;
import flike.words.Word;

/**
 * A placeholder used purely during the compilation of If-(Else)-Then conditionals.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class IfPlaceholder extends Word{

    @Override
    public void exec(ExecContainer ec) throws MissingWordException {
        throw new MissingWordException("In case you see this, something extremely horrible has happened with the FLike compiler.");
    }

    @Override
    public String toString() {
        return "IF PLACEHOLDER";
    }
    
}
