package flike.words.placeholders;

import flike.ExecContainer;
import flike.exceptions.MissingWordException;
import flike.words.Word;

/**
 * A placeholder used to store identifiers.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class IdentifierPlaceholder extends Word {

    final String phldr;

    public IdentifierPlaceholder() {
        this.phldr = "";
    }

    public IdentifierPlaceholder(String phldr) {
        this.phldr = phldr;
    }

    @Override
    public void exec(ExecContainer ec) throws MissingWordException {
        throw new MissingWordException("MISSING " + phldr + "  In case you see this, something extremely horrible has happened with the FLike compiler.");
    }

    @Override
    public String toString() {
        return "CALL PLACEHOLDER " + phldr;
    }

    public String getPlaceholder() {
        return phldr;
    }

}
