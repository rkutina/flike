package flike.words.placeholders;

import flike.words.*;
import flike.ExecContainer;
import flike.exceptions.MissingWordException;

/**
 * A placeholder for an upcoming Do loop. Should never end up being executed.
 * 
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class DoPlaceholder extends Word {

    @Override
    public void exec(ExecContainer ec) throws MissingWordException {
        throw new MissingWordException("In case you see this, something extremely horrible has happened with the FLike compiler.");
    }    

    @Override
    public String toString() {
        return "DO PLACEHOLDER";
    }

}
