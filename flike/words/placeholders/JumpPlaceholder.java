package flike.words.placeholders;

import flike.ExecContainer;
import flike.exceptions.MissingWordException;
import flike.words.Word;

/**
 * A placeholder used purely during the compilation. Meant for random jumps and always loops.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class JumpPlaceholder extends Word {
    
    final String phldr;
    
    public JumpPlaceholder(){
        this.phldr = "";
    }
    
    public JumpPlaceholder(String phldr){
        this.phldr = phldr;
    }
    
    @Override
    public void exec(ExecContainer ec) throws MissingWordException {
        throw new MissingWordException("In case you see this, something extremely horrible has happened with the FLike compiler.");
    }

    @Override
    public String toString() {
        return "JUMP PLACEHOLDER " + phldr;
    }
    
    public String getPlaceholder(){
        return phldr;
    }

}
