package flike.words;

import flike.ExecContainer;

/**
 * CallWord. Used for jumping to a given address, and returning back to
 * execution beyond this point later.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class CallWord extends Word {

    final int cptr;

    public CallWord(int cptr) {
        this.cptr = cptr;
    }

    @Override
    public void exec(ExecContainer ec) {
        ec.rStack.push(ec.ptr);

        ec.ptr = this.cptr;
    }

    @Override
    public String toString() {
        return "CALL " + cptr;
    }

}
