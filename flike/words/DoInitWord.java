package flike.words;

import flike.ExecContainer;
import static flike.HelperFunctions.assertStackDepth;
import flike.exceptions.StackUnderflowException;
import flike.exceptions.TypeException;

public class DoInitWord extends Word {

    public DoInitWord() {

    }

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException, TypeException {
        assertStackDepth(ec.dStack, 2);

        if (ec.dStack.peek() instanceof Long) {
            ec.iStack.push((Long) ec.dStack.pop()-1);
        } else {
            throw new TypeException("DoInit: Expected Long, found " + ec.dStack.peek().getClass().toString());
        }
        
        if (ec.dStack.peek() instanceof Long) {
            ec.iStack.push((Long) ec.dStack.pop());
        } else {
            throw new TypeException("DoInit: Expected Long, found " + ec.dStack.peek().getClass().toString());
        }
        
        ec.ptr++;
    }

    @Override
    public String toString() {
        return "DO INIT";
    }

}
