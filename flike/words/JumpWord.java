package flike.words;

import flike.ExecContainer;
import flike.exceptions.StackUnderflowException;

/**
 * JumpWord. Unconditionally jumps to a given address.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class JumpWord extends Word {

    private final int jptr;

    public JumpWord(int ptr) {
        this.jptr = ptr;
    }

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException {
        ec.ptr = this.jptr;
    }

    @Override
    public String toString() {
        return "JMP " + this.jptr;
    }

}
