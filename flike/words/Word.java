package flike.words;

import flike.ExecContainer;
import flike.exceptions.FLikeExecException;
import flike.exceptions.MissingWordException;
import flike.exceptions.OutOfBoundsException;
import flike.exceptions.StackUnderflowException;
import flike.exceptions.TypeException;
import flike.exceptions.UnknownWordException;

/**
 * An interface to all the Word definitions. In case you wonder why in Todd's
 * name it's an abstract class instead of an actual interface, then well, let's
 * say I'm totally future-proofing this. I might actually want to store some
 * extra data in each word to aid debugging later.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public abstract class Word {

    /**
     * Exec function. This function simply gets executed when this word is meant
     * to be run.
     *
     * @param ec ExecContainer containing just about everything everything.
     *
     * @throws StackUnderflowException Thrown on various occasions.
     * @throws FLikeExecException Thrown on various occasions.
     * @throws MissingWordException Thrown on various occasions.
     * @throws OutOfBoundsException Thrown on various occasions.
     * @throws TypeException Thrown on various occasions.
     * @throws UnknownWordException Thrown on various occasions.
     */
    public abstract void exec(ExecContainer ec)
            throws StackUnderflowException, FLikeExecException, MissingWordException, OutOfBoundsException, TypeException, UnknownWordException;

    /**
     * toString. Will probably return a string of some fairly useful information
     * about each Word. Or well, that's what I meant to use this for.
     *
     * @return
     */
    @Override
    public abstract String toString();

}
