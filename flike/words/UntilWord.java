package flike.words;

import static flike.HelperFunctions.assertStackDepth;
import flike.ExecContainer;
import static flike.HelperFunctions.debug;
import flike.exceptions.StackUnderflowException;
import flike.exceptions.TypeException;

/**
 * UntilWord. Used for implementing the Begin-Until construct. Loops to a given
 * address until the value (Long) on top of dStack (consumed) is nonzero.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class UntilWord extends Word {

    private final int jptr;

    public UntilWord(int jptr) {
        this.jptr = jptr;
    }

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException, TypeException {

        assertStackDepth(ec.dStack, 1);

        Object pop = ec.dStack.pop();

        if (pop instanceof Long) {
            debug("UNTIL COMPARISON");

            if ((Long) pop == 0) {
                debug("\tFALSE");
                debug("\t:JMP (" + jptr + ")");
                ec.ptr = this.jptr;

            } else {
                debug("\tTRUE");
                ec.ptr++;
            }

        } else {
            throw new TypeException("IF: expected Long, got " + pop.getClass().getCanonicalName());
        }
    }

    @Override
    public String toString() {
        return "LOOP " + this.jptr;
    }

}
