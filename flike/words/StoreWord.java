package flike.words;

import flike.ExecContainer;
import static flike.HelperFunctions.*;
import flike.exceptions.StackUnderflowException;
import flike.exceptions.UnknownWordException;
import java.util.HashMap;

/**
 * StoreWord. Defines the behavior of the store operator.
 * 
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class StoreWord extends Word {

    private final String identifier;
    private final boolean isGlobal;
    
    public StoreWord(String identifier, boolean isGlobal){
        this.identifier = identifier;
        this.isGlobal = isGlobal;
    }
    
    @Override
    public void exec(ExecContainer ec) throws UnknownWordException, StackUnderflowException {
        HashMap<String, Object> varcontainer = (isGlobal) ? ec.globvars : ec.vars;
        
        assertKeyExists(varcontainer, this.identifier);
        assertStackDepth(ec.dStack, 1);
        
        varcontainer.put(identifier, ec.dStack.pop());  //should it be pop or peek?
        
        ec.ptr++;
    }

    @Override
    public String toString() {
        return "PW Store " + ((isGlobal)?"G":"L") +  "\'" + identifier + "\'";
    }
    
}
