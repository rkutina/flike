package flike.words;

import flike.ExecContainer;
import static flike.HelperFunctions.*;
import flike.exceptions.StackUnderflowException;
import flike.exceptions.TypeException;

/**
 * IfWord. Used for compiling If-Then-Else constructs. Check for a Long on top
 * of the dStack, if it's nonzero, continues code execution, if it's zero, jumps
 * to either else or then, whichever is closer.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class IfWord extends Word {

    private final int ptrThen;
    private final int ptrElse;

    public IfWord(int ptrThen, int ptrElse) {
        this.ptrThen = ptrThen;
        this.ptrElse = ptrElse;
    }

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException, TypeException {

        assertStackDepth(ec.dStack, 1);

        Object pop = ec.dStack.pop();

        if (pop instanceof Long) {
            debug("IF COMPARISON");

            if ((Long) pop == 0) {
                debug("\tFALSE");
                if (ptrElse != -1) {
                    debug("\t:JMP ELSE (" + ptrElse + ")");
                    ec.ptr = this.ptrElse;
                } else {
                    debug("\t:JMP THEN (" + ptrThen + ")");
                    ec.ptr = this.ptrThen;
                }
            } else {
                debug("\tTRUE");
                ec.ptr++;
            }

        } else {
            throw new TypeException("IF: expected Long, got " + pop.getClass().getCanonicalName());
        }
    }

    @Override
    public String toString() {
        return "IF e:" + this.ptrElse + " t:" + this.ptrThen;
    }

}
