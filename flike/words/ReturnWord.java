/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flike.words;

import flike.ExecContainer;
import static flike.HelperFunctions.assertStackDepth;
import flike.exceptions.StackUnderflowException;

/**
 * ReturnWord. Used to return from calls. To see how that works, read the docs
 * of CallWord, or just think about it for a second. preferably do both.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class ReturnWord extends Word {

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException {
        assertStackDepth(ec.rStack, 1);

        ec.ptr = ec.rStack.pop()+1;
    }

    @Override
    public String toString() {
        return "RET";
    }

}
