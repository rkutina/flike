package flike.words;

import flike.ExecContainer;

/**
 * LiteralWord class. Used for storing literal values prior to execution. Pushes
 * its value to dStack when executed. Surprising, isn't it?
 *
 * @author rkutina
 */
public class LiteralWord extends Word {

    private final Object value;

    public LiteralWord(Object value) {
        this.value = value;
    }

    @Override
    public void exec(ExecContainer ec) {
        ec.dStack.push(value);
        ec.ptr++;
    }

    @Override
    public String toString() {
        return "LIT " + value.toString();
    }

}
