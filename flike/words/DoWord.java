package flike.words;

import flike.ExecContainer;
import static flike.HelperFunctions.assertStackDepth;
import flike.exceptions.StackUnderflowException;

public class DoWord extends Word {

    private final int endptr;

    public DoWord(int endptr) {
        this.endptr = endptr;
    }

    @Override
    public void exec(ExecContainer ec) throws StackUnderflowException {
        assertStackDepth(ec.iStack, 2);
        
        long lim = ec.iStack.pop();
        long cur = ec.iStack.pop()+1;
        
        if(cur < lim){
            ec.iStack.push(cur);
            ec.iStack.push(lim);
            ec.rStack.push(ec.ptr-1);
            ec.ptr++;
        }else{
            ec.ptr = endptr;
        }
    }

    @Override
    public String toString() {
        return "DO " + endptr;
    }

}
