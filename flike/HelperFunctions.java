package flike;

import flike.exceptions.*;
import flike.tokenizer.FLikeTokenizer;
import tokenizer.Token;
import java.util.HashMap;
import java.util.Stack;

/**
 * A class containing a whole bunch of totally useful functions, used mostly for
 * debug and oops-prevention/exception generation.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class HelperFunctions {

    /**
     * Used to test if there's enough stuff on the stack to pop/peek into.
     *
     * @param s The stack to test.
     * @param depth The number of items that should be on the stack.
     * @throws StackUnderflowException Only when there is less items on the
     * stack than the number specified.
     */
    public static void assertStackDepth(Stack s, int depth) throws StackUnderflowException {
        if (s.toArray().length < depth) {
            throw new StackUnderflowException();
        }
    }

    /**
     * Used to test if there's enough items on an array after a specified index
     *
     * @param arr The array to test.
     * @param index An index where to start.
     * @param num A number of cells there should be after the index.
     * @throws OutOfBoundsException Thrown when there isn't enough items on the
     * specified array.
     */
    public static void assertArrayLength(Object[] arr, int index, int num) throws OutOfBoundsException {
        if (arr.length <= (index + num)) {
            throw new OutOfBoundsException();
        }
    }

    /**
     * Tests if a key in a given HashMap exists. Only used to check whether a
     * variable exists or not.
     *
     * @param vars The HashMap of variables to test.
     * @param name The key to look for
     * @throws UnknownWordException Thrown when the specified key doesn't exist
     * in the specified HashMap.
     */
    public static void assertKeyExists(HashMap<String, Object> vars, String name) throws UnknownWordException {
        if (!vars.keySet().contains(name)) {
            throw new UnknownWordException();
        }
    }

    /**
     * A function I used for debugging. Contains just a commented out println,
     * so this function does basically nothing in normal operation. Uncomment to
     * get some debug info. It's probably mostly unused now anyway.
     *
     * @param s A string to print out.
     */
    public static void debug(String s) {
        System.out.println("\t" + s);
    }

    /**
     * Converts a Verilog-like number literal format string to a Long.
     *
     * @param s A string containing the Verilog-like formatted numeric literal
     * @return Returns a converted long.
     * @throws ConversionException Thrown when the given string doesn't match
     * the specified format or something like that.
     */
    public static long longFromVerilogLike(String s) throws ConversionException {
        long ret = 0;

        if (s.matches("[0-9]+'[bohd]?[0-9a-f]+")) {
            int apos = s.indexOf('\'');

            int width = Integer.parseInt(s.substring(0, apos));

            if (width > 64) {
                throw new ConversionException("Converting a v-like form \'" + s + "\': cannot use bit widths larger than 64 in this version!");
            }

            char base = s.charAt(apos + 1);

            String val = s.substring(apos + 2);

            int radix;

            switch (base) {
                case 'B':   //bin
                case 'b': {
                    radix = 2;
                    break;
                }
                case 'O':   //oct
                case 'o': {
                    radix = 8;
                    break;
                }
                case 'H':   //hex
                case 'h': {
                    radix = 16;
                    break;
                }
                case 'D':   //dec
                case 'd': {
                    radix = 10;
                    break;
                }
                default: {
                    radix = 10;
                    val = s.substring(apos + 1);
                    break;
                }
            }

            ret = Long.parseLong(val, radix);

            ret = ret & ((~0L) >>> (64 - width));

        } else {
            throw new ConversionException("Converting a v-like form \'" + s + "\': invalid form!");
        }
        return ret;
    }

    /**
     * A function used to check whether a given Token is of the given type.
     *
     * @param t A token to check
     * @param tt A token type to check for
     * @return Returns true when a given Token is of the given type.
     */
    public static boolean matchesTokType(Token t, FLikeTokenizer.TokenType tt) {
        return t.getTokenID() == tt.getTokenID();
    }
    
    public static void debugDump(ExecContainer ec){
        System.out.println("--- EXEC DEBUG DUMP @" + System.currentTimeMillis()/1000 + " ---");
        
        System.out.println("D-Stack:");
        int i = 0;
        if(ec.dStack.size()==0){System.out.println("\tEmpty.");}
        for(Object o : ec.dStack){
            System.out.println("\t-" + i++ + "\t" + o.toString());
        }
        System.out.println();
        
        System.out.println("SD-Stack:");
        i = 0;
        if(ec.sdStack.size()==0){System.out.println("\tEmpty.");}
        for(Object o : ec.sdStack){
            System.out.println("\t-" + i++ + "\t" + o.toString());
        }
        
        System.out.println();
        
        System.out.println("R-Stack:");
        i = 0;
        if(ec.rStack.size()==0){System.out.println("\tEmpty.");}
        for(Integer ri : ec.rStack){
            System.out.println("\t-" + i++ + "\t" + ri);
        }
        
        System.out.println("--- END OF DEBUG DUMP ---");
    }
}
