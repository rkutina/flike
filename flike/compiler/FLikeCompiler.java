package flike.compiler;

import flike.compiler.wordcompilers.*;
import flike.compiler.wordcompilers.doloop.*;
import flike.compiler.wordcompilers.literals.*;
import flike.compiler.wordcompilers.worddefs.*;
import flike.compiler.wordcompilers.ifthenelse.*;
import flike.compiler.wordcompilers.beginuntil.*;

import tokenizer.Token;
import flike.tokenizer.FLikeTokenizer.TokenType;

import flike.exceptions.CompilerException;

import static flike.HelperFunctions.debug;
import static flike.HelperFunctions.matchesTokType;

import flike.words.*;
import flike.words.primitive.PrimitiveWords;
import flike.words.placeholders.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

/**
 * FLikeCompiler compiles the tokens got from the FLikeTokenizer to an array of
 * directly executable words. Handy for executing the same stuff multiple times,
 * as that would be probably the case fairly often.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class FLikeCompiler {

    public class CompilerContainer {

        /**
         * ArrayList containing the compiled words.
         */
        public ArrayList<Word> compiled;

        /**
         * HashMap containing all the global word definitions.
         */
        public HashMap<String, Word[]> globals;

        public int cptr = -1;

        public Stack<Integer> wordPtrs = new Stack<>();
        public Stack<Integer> loopPtrs = new Stack<>();

        public HashMap<String, Integer> wordPtrMap = new HashMap<>();

        public HashMap<String, PrimitiveWord> pwrds = new PrimitiveWords().getWords();
        
        public Token[] tokens;
    }

    private final CompilerContainer cc;

    public FLikeCompiler() {
        this.cc = new CompilerContainer();
        cc.globals = new HashMap<>();
        cc.compiled = new ArrayList<>();
    }

    /**
     * Compiles the given Token array into something executable.
     *
     * @param tokens The token array to compile.
     * @throws CompilerException Thrown when something goes oops.
     */
    public void compile(Token[] tokens) throws CompilerException {

        debug("--- tokens to compile ---");
        for (Token x : tokens) {
            debug("" + x.getTokenID() + "\t" + x.getSequence());
        }
        debug("--- that's all of them --- \n");

        cc.compiled = new ArrayList<>();
        cc.tokens = tokens;

        while (cc.cptr < (tokens.length - 1)) {
            Token t = tokens[++cc.cptr];
            
            WordCompilerIF com = null;
            
            if (matchesTokType(t, TokenType.NULL)) com = new WordCompilerIF() {
                @Override
                public void compile(Token t, CompilerContainer cc) throws CompilerException {
                    debug("Null");
                }
            };
            if (matchesTokType(t, TokenType.LONGLITERAL)) com = new LongLiteralWordCompiler();
            if (matchesTokType(t, TokenType.FLOATLITERAL)) com = new FloatLiteralWordCompiler();
            if (matchesTokType(t, TokenType.BITLONGLITERAL)) com = new BitLongLiteralWordCompiler();
            if (matchesTokType(t, TokenType.STRINGLITERAL)) com = new StringLiteralWordCompiler();
            
            if (matchesTokType(t, TokenType.VARALLOC)) com = new VarallocWordCompiler();
            if (matchesTokType(t, TokenType.VAROPS)) com = new VarOpWordCompiler();
            
            if (matchesTokType(t, TokenType.WORDDEF_BEGIN)) com = new StartWordDefWordCompiler();
            if (matchesTokType(t, TokenType.WORDDEF_END)) com = new EndWordDefWordCompiler();
            if (matchesTokType(t, TokenType.WORDDEF_BEGIN_GLOB)) com = new GlobWordWordCompiler();
            if (matchesTokType(t, TokenType.WORDDEF_END_GLOB)) com = new WordCompilerIF() {
                @Override
                public void compile(Token t, CompilerContainer cc) throws CompilerException {
                throw new CompilerException("GlobWorddef: found a lonely enddef. How sad.");
                }
            };
            
            if (matchesTokType(t, TokenType.IF)) com = new IfWordCompiler();
            if (matchesTokType(t, TokenType.ELSE)) com = new ElseWordCompiler();
            if (matchesTokType(t, TokenType.THEN)) com = new ThenWordCompiler();
            
            if (matchesTokType(t, TokenType.DO)) com = new DoWordCompiler();
            if (matchesTokType(t, TokenType.LOOP)) com = new LoopWordCompiler();
            
            if (matchesTokType(t, TokenType.BEGIN)) com = new BeginWordCompiler();
            if (matchesTokType(t, TokenType.UNTIL)) com = new UntilWordCompiler();
            if (matchesTokType(t, TokenType.FOREVER)) com = new ForeverWordCompiler();
            if (matchesTokType(t, TokenType.LEAVE)) com = new LeaveWordCompiler();
            
            if (matchesTokType(t, TokenType.IDENTIFIER)) com = new IdentifierWordCompiler();
            
            //TODO: handle some more loopy stuff
            
            if(com == null){
                throw new UnsupportedOperationException("Unsupported token: " + t.getTokenID() + " (" + t.getSequence() + ")");
            }else{
                com.compile(t, cc);
            }
        }

        //'link' words to leftover identifier placeholders
        for (int i = 0; i < cc.compiled.size(); i++) {
            Word w = cc.compiled.get(i);
            if (w instanceof IdentifierPlaceholder) {
                String identifier = ((IdentifierPlaceholder) w).getPlaceholder();

                if (cc.wordPtrMap.containsKey(identifier)) {
                    cc.compiled.set(i, new CallWord(cc.wordPtrMap.get(identifier)));
                    debug("Assigning adr to a word");
                } else if (cc.globals.containsKey(identifier)) {
                    debug("Now I should handle calling global words somehow...");
                    cc.compiled.set(i, new CallGlobal(identifier));
                } else {
                    debug("No idea where the fuck \'" + identifier + "\' is.");
                }
            }
        }

        debug("");
        debug("Resulting cc.compiled thing:");
        int p = 0;
        for (Word w : cc.compiled) {
            debug(p++ + ": " + w.toString());
        }

        //this.selftest();
    }

    /**
     * Returns an array of compiled words.
     *
     * @return An array of compiled words.
     */
    public Word[] getCompiled() {
        return cc.compiled.toArray(new Word[cc.compiled.size()]);
    }

    /**
     * Returns an array of compiled global words.
     * 
     * @return An array of compiled global words.
     */
    public HashMap<String, Word[]> getGlobals() {
        return cc.globals;
    }
}
