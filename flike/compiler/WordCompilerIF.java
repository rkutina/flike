package flike.compiler;

import flike.compiler.FLikeCompiler.CompilerContainer;
import flike.exceptions.CompilerException;
import tokenizer.Token;

/**
 * An interface used by all the word compilers.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public interface WordCompilerIF {

    /**
     * The function to call when one wants to compile stuff into words.
     *
     * @param t Token to compile into a word.
     * @param cc CompilerContainer containing everything required for
     * compilation.
     * @throws CompilerException Thrown when something goes horribly wrong.
     */
    public void compile(Token t, CompilerContainer cc) throws CompilerException;
}
