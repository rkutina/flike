package flike.compiler.wordcompilers;

import static flike.HelperFunctions.debug;
import flike.compiler.WordCompilerIF;
import flike.compiler.FLikeCompiler;
import flike.exceptions.CompilerException;
import tokenizer.Token;
import flike.words.CallWord;
import flike.words.placeholders.IdentifierPlaceholder;

/**
 * Meant to compile all sorts of identifiers into something runnable. If that's
 * not possible, just slap in a placeholder to link/consume later.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class IdentifierWordCompiler implements WordCompilerIF {

    @Override
    public void compile(Token t, FLikeCompiler.CompilerContainer cc) throws CompilerException {

        String identifier = t.getSequence().toLowerCase();

        if (cc.pwrds.containsKey(identifier)) {
            debug("Primitive " + identifier);
            cc.compiled.add(cc.pwrds.get(identifier));
        } else if (cc.wordPtrMap.containsKey(identifier)) {
            debug("NonPrimitive " + identifier);
            cc.compiled.add(new CallWord(cc.wordPtrMap.get(identifier)));
        } else {
            debug("Unknown " + identifier);
            cc.compiled.add(new IdentifierPlaceholder(identifier));
        }
    }

}
