package flike.compiler.wordcompilers.beginuntil;

import static flike.HelperFunctions.assertStackDepth;
import flike.compiler.FLikeCompiler;
import flike.compiler.WordCompilerIF;
import flike.exceptions.CompilerException;
import flike.exceptions.StackUnderflowException;
import tokenizer.Token;
import flike.words.UntilCond;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class UntilWordCompiler implements WordCompilerIF {

    @Override
    public void compile(Token t, FLikeCompiler.CompilerContainer cc) throws CompilerException {
        
        try {
            assertStackDepth(cc.loopPtrs, 1);
        } catch (StackUnderflowException ex) {
            throw new CompilerException("Found an Until with no Begin!");
        }
        
        cc.compiled.add(new UntilCond(cc.loopPtrs.pop()));
    }
    
}
