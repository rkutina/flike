package flike.compiler.wordcompilers.beginuntil;

import flike.compiler.FLikeCompiler;
import flike.compiler.WordCompilerIF;
import flike.exceptions.CompilerException;
import tokenizer.Token;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class BeginWordCompiler implements WordCompilerIF {

    @Override
    public void compile(Token t, FLikeCompiler.CompilerContainer cc) throws CompilerException {
        cc.loopPtrs.push(cc.compiled.size());
    }
    
}
