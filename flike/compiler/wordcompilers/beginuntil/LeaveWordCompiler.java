package flike.compiler.wordcompilers.beginuntil;

import static flike.HelperFunctions.matchesTokType;
import flike.compiler.FLikeCompiler;
import flike.compiler.WordCompilerIF;
import flike.exceptions.CompilerException;
import flike.tokenizer.FLikeTokenizer;
import tokenizer.Token;
import flike.words.JumpWord;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class LeaveWordCompiler implements WordCompilerIF {

    @Override
    public void compile(Token t, FLikeCompiler.CompilerContainer cc) throws CompilerException {
        int cptr = cc.cptr;
        while (true) {
            if (cc.tokens.length-1 <= cptr) {
                break;
            }
            if (matchesTokType(cc.tokens[++cptr], FLikeTokenizer.TokenType.UNTIL)
                    | matchesTokType(t, FLikeTokenizer.TokenType.FOREVER)) {
                break;
            }
            if (matchesTokType(t, FLikeTokenizer.TokenType.WORDDEF_END) | matchesTokType(t, FLikeTokenizer.TokenType.WORDDEF_END_GLOB)) {
                throw new CompilerException("Cannot leave a worddef in this version.");
            }
        }

        cc.compiled.add(new JumpWord(cptr));
    }

}
