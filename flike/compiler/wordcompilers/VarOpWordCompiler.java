package flike.compiler.wordcompilers;

import flike.compiler.WordCompilerIF;
import flike.compiler.FLikeCompiler;
import flike.exceptions.CompilerException;
import tokenizer.Token;
import flike.words.FetchWord;
import flike.words.IncDecVarWord;
import flike.words.StoreWord;
import flike.words.placeholders.IdentifierPlaceholder;

/**
 * Meant to compile all sorts of variable operations into something executable.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class VarOpWordCompiler implements WordCompilerIF {

    @Override
    public void compile(Token t, FLikeCompiler.CompilerContainer cc) throws CompilerException {
        String s = t.getSequence();

        String identifier;
        int i = cc.compiled.size() - 1;

        if ((cc.compiled.size() > 0) && (cc.compiled.get(i) instanceof IdentifierPlaceholder)) {
            identifier = ((IdentifierPlaceholder) cc.compiled.get(i)).getPlaceholder();
            cc.compiled.remove(i);
        } else {
            throw new CompilerException("VAROPS: Cannot find a preceding identifier!");
        }

        boolean incDec = false;
        boolean isIncDec;

        boolean isGlob = (s.replaceAll("[^@!]", "").length() == 2);

        if (isIncDec = s.matches("[+-][@!][@!]?")) {
            incDec = (s.charAt(0) == '+');
        }

        switch (s.replaceAll("[+-]", "")) {
            case "@":
            case "@@":
                if(isIncDec){
                    cc.compiled.add(new IncDecVarWord(identifier, isGlob, incDec));
                }
                cc.compiled.add(new FetchWord(identifier, isGlob));
                break;
            case "!":
            case "!!":
                if(isIncDec){
                    cc.compiled.add(new IncDecVarWord(identifier, isGlob, incDec));
                }else{
                    cc.compiled.add(new StoreWord(identifier, isGlob));
                }
                break;
            default:
                throw new CompilerException("VAROPS: WTF?!");
        }

    }
}
