package flike.compiler.wordcompilers.ifthenelse;

import flike.compiler.WordCompilerIF;
import flike.compiler.FLikeCompiler;
import flike.exceptions.CompilerException;
import tokenizer.Token;
import flike.words.IfWord;
import flike.words.JumpWord;
import flike.words.Word;
import flike.words.placeholders.IfPlaceholder;
import flike.words.placeholders.JumpPlaceholder;

/**
 * Meant to compile 'Then' word into something usable. Also noms the previous
 * Else and If placeholders and links them properly.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class ThenWordCompiler implements WordCompilerIF {

    @Override
    public void compile(Token t, FLikeCompiler.CompilerContainer cc) throws CompilerException {
        int lookbackptr = cc.compiled.size();
        int ptrElse = -1;

        while (lookbackptr > 1) {
            lookbackptr--;

            Word w = cc.compiled.get(lookbackptr);

            if (w instanceof JumpPlaceholder) {
                if ("ELSE".equals(((JumpPlaceholder) w).getPlaceholder())) {
                    if (ptrElse != -1) {
                        throw new CompilerException("IfThenElse: Found multiple elses for one if!");
                    }

                    cc.compiled.set(lookbackptr, new JumpWord(cc.compiled.size()));
                    ptrElse = lookbackptr + 1;
                }
            } else if (w instanceof IfPlaceholder) {
                cc.compiled.set(lookbackptr, new IfWord(cc.compiled.size(), ptrElse));
            }
        }
    }

}
