package flike.compiler.wordcompilers.ifthenelse;

import flike.compiler.WordCompilerIF;
import flike.compiler.FLikeCompiler;
import flike.exceptions.CompilerException;
import tokenizer.Token;
import flike.words.placeholders.IfPlaceholder;

/**
 * Meant to compile an 'If' word into an If placeholder.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class IfWordCompiler implements WordCompilerIF {

    @Override
    public void compile(Token t, FLikeCompiler.CompilerContainer cc) throws CompilerException {
        cc.compiled.add(new IfPlaceholder());
    }

}
