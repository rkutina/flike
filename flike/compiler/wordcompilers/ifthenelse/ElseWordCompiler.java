package flike.compiler.wordcompilers.ifthenelse;

import flike.compiler.WordCompilerIF;
import flike.compiler.FLikeCompiler;
import flike.exceptions.CompilerException;
import tokenizer.Token;
import flike.words.placeholders.JumpPlaceholder;

/**
 * Meant to add a jump placeholder for an 'Else' word.
 * 
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class ElseWordCompiler implements WordCompilerIF {

    @Override
    public void compile(Token t, FLikeCompiler.CompilerContainer cc) throws CompilerException {
        cc.compiled.add(new JumpPlaceholder("ELSE"));
    }

}
