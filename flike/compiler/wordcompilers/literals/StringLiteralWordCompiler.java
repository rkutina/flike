package flike.compiler.wordcompilers.literals;

import flike.compiler.WordCompilerIF;
import flike.compiler.FLikeCompiler;
import static flike.HelperFunctions.debug;
import flike.exceptions.CompilerException;
import tokenizer.Token;
import flike.words.LiteralWord;

/**
 * Meant to compile into a string literal word.
 * 
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class StringLiteralWordCompiler implements WordCompilerIF {

    @Override
    public void compile(Token t, FLikeCompiler.CompilerContainer cc) throws CompilerException {
        debug("Found a string literal: " + t.getSequence());

        cc.compiled.add(new LiteralWord(t.getSequence().substring(1, t.getSequence().length()-1)));
    }

}
