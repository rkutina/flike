package flike.compiler.wordcompilers.literals;

import flike.compiler.WordCompilerIF;
import flike.compiler.FLikeCompiler;
import static flike.HelperFunctions.debug;
import static flike.HelperFunctions.longFromVerilogLike;
import flike.exceptions.CompilerException;
import flike.exceptions.ConversionException;
import tokenizer.Token;
import flike.words.LiteralWord;

/**
 * Meant to compile into a literal thing.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class BitLongLiteralWordCompiler implements WordCompilerIF {

    @Override
    public void compile(Token t, FLikeCompiler.CompilerContainer cc) throws CompilerException {
        debug("Found a bitlongliteral: " + t.getSequence());

        try {
            cc.compiled.add(new LiteralWord(longFromVerilogLike(t.getSequence())));
        } catch (ConversionException ex) {
            throw new CompilerException(ex.getMessage());
        }
    }

}
