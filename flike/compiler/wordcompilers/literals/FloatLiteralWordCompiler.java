package flike.compiler.wordcompilers.literals;

import flike.compiler.WordCompilerIF;
import flike.compiler.FLikeCompiler;
import static flike.HelperFunctions.debug;
import flike.exceptions.CompilerException;
import tokenizer.Token;
import flike.words.LiteralWord;

/**
 * Meant to compile into a float literal thing.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class FloatLiteralWordCompiler implements WordCompilerIF {

    @Override
    public void compile(Token t, FLikeCompiler.CompilerContainer cc) throws CompilerException {
        debug("Found a floatliteral: " + t.getSequence());

        cc.compiled.add(new LiteralWord(Double.parseDouble(t.getSequence())));
    }

}
