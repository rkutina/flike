package flike.compiler.wordcompilers.doloop;

import flike.compiler.WordCompilerIF;
import flike.compiler.FLikeCompiler;
import flike.exceptions.CompilerException;
import tokenizer.Token;
import flike.words.DoInitWord;
import flike.words.placeholders.DoPlaceholder;

/**
 * Meant to compile the 'Do' word into something fairly usable.
 * 
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class DoWordCompiler implements WordCompilerIF {

    @Override
    public void compile(Token t, FLikeCompiler.CompilerContainer cc) throws CompilerException {
        cc.compiled.add(new DoInitWord());
        cc.loopPtrs.push(cc.compiled.size());
        cc.compiled.add(new DoPlaceholder());
    }

}
