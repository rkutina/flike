package flike.compiler.wordcompilers.doloop;

import flike.compiler.WordCompilerIF;
import flike.compiler.FLikeCompiler;
import flike.exceptions.CompilerException;
import tokenizer.Token;
import flike.words.DoWord;
import flike.words.ReturnWord;

/**
 * Meant to compile the 'Loop' word into something fairly usable.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class LoopWordCompiler implements WordCompilerIF {

    @Override
    public void compile(Token t, FLikeCompiler.CompilerContainer cc) throws CompilerException {
        cc.compiled.add(new ReturnWord());

        if (cc.loopPtrs.isEmpty()) {
            throw new CompilerException("Loop: I'm probably lonely here.");
        } else {
            cc.compiled.set(cc.loopPtrs.pop(), new DoWord(cc.compiled.size()));
        }
    }

}
