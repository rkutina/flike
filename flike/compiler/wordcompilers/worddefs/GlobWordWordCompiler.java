package flike.compiler.wordcompilers.worddefs;

import flike.compiler.WordCompilerIF;
import flike.compiler.FLikeCompiler;
import static flike.HelperFunctions.debug;
import static flike.HelperFunctions.matchesTokType;
import flike.exceptions.CompilerException;
import flike.tokenizer.FLikeTokenizer;
import tokenizer.Token;
import java.util.ArrayList;

/**
 * Meant to compile the glob word definition thing.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class GlobWordWordCompiler implements WordCompilerIF {

    @Override
    public void compile(Token t, FLikeCompiler.CompilerContainer cc) throws CompilerException {
        Token ti = cc.tokens[++cc.cptr];

        if (!matchesTokType(ti, FLikeTokenizer.TokenType.IDENTIFIER)) {
            throw new CompilerException("GlobWorddef: an identifier was expected after the worddef token");
        } else {
            String wordname = ti.getSequence().toLowerCase();

            if (cc.globals.containsKey(wordname)) {
                throw new CompilerException("GlobWorddef: trying to redefine a global word \'" + wordname + "\'");
            }

            int p = cc.cptr + 1;

            ArrayList<Token> gwtokens;
            gwtokens = new ArrayList<>();

            //get all the tokens that should belong to this word's definition
            while (p < (cc.tokens.length - 1)
                    && !matchesTokType(cc.tokens[p], FLikeTokenizer.TokenType.WORDDEF_END_GLOB)) {
                debug("Adding " + cc.tokens[p].getSequence() + " to token list.");
                gwtokens.add(cc.tokens[p]);
                p++;
            }

            //throw an exception in case you just happened to run out of tokens before finding the end stuff
            if (!(p < (cc.tokens.length - 1))) {
                throw new CompilerException("GlobWorddef: can't find a matching enddef");
            }

            //replace the end token with a NULL, so in case it would get somehow iterated over, it won't throw a false alarm exception
            cc.tokens[p] = new Token(FLikeTokenizer.TokenType.NULL.getTokenID(), ";; repl");
            cc.cptr = p;

            //compile the word's tokens
            FLikeCompiler fc = new FLikeCompiler();
            fc.compile(gwtokens.toArray(new Token[gwtokens.size()])); //woohoo, everyone loves recursion...

            //slap the resulting word array onto the global word definition hashmap
            cc.globals.put(wordname, fc.getCompiled());
        }
    }

}
