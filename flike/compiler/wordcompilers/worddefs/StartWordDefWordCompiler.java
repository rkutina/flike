package flike.compiler.wordcompilers.worddefs;

import flike.compiler.WordCompilerIF;
import flike.compiler.FLikeCompiler;
import static flike.HelperFunctions.debug;
import static flike.HelperFunctions.matchesTokType;
import flike.exceptions.CompilerException;
import flike.tokenizer.FLikeTokenizer;
import tokenizer.Token;
import flike.words.placeholders.IdentifierPlaceholder;

/**
 * Meant to compile word def. somehow.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class StartWordDefWordCompiler implements WordCompilerIF {

    @Override
    public void compile(Token t, FLikeCompiler.CompilerContainer cc) throws CompilerException {

        Token ti = cc.tokens[++cc.cptr];

        if (matchesTokType(ti, FLikeTokenizer.TokenType.IDENTIFIER)) {

            String wordname = ti.getSequence().toLowerCase();

            if (cc.globals.containsKey(wordname) || cc.pwrds.containsKey(wordname)) {
                throw new CompilerException("Worddef: trying to redefine a primitive/global word + \'" + wordname + "\'");
            }

            debug("Worddef: adding a new word \'" + wordname + "\'");

            cc.wordPtrs.push(cc.compiled.size());  //add a pointer referencing this place to the word pointer map thing.

            cc.compiled.add(new IdentifierPlaceholder());  //placeholder, an actual value will be fitted in afterwards

            cc.wordPtrMap.put(wordname, cc.compiled.size());

        } else {
            throw new CompilerException("Worddef found with an invalid identifier " + ti.getTokenID() + " " + ti.getSequence());
        }
    }

}
