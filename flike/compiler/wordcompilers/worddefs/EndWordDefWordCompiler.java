package flike.compiler.wordcompilers.worddefs;

import flike.compiler.WordCompilerIF;
import flike.compiler.FLikeCompiler;
import static flike.HelperFunctions.debug;
import flike.exceptions.CompilerException;
import tokenizer.Token;
import flike.words.JumpWord;
import flike.words.ReturnWord;

/**
 * Meant to compile the word def. end word into a return word.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class EndWordDefWordCompiler implements WordCompilerIF {

    @Override
    public void compile(Token t, FLikeCompiler.CompilerContainer cc) throws CompilerException {
        int wordindex = cc.wordPtrs.pop();

        debug("Worddef end");

        cc.compiled.add(new ReturnWord());
        cc.compiled.set(wordindex, new JumpWord(cc.compiled.size()));
    }

}
