package flike.compiler.wordcompilers;

import flike.compiler.WordCompilerIF;
import flike.compiler.FLikeCompiler;
import static flike.HelperFunctions.debug;
import static flike.HelperFunctions.matchesTokType;
import flike.exceptions.CompilerException;
import flike.tokenizer.FLikeTokenizer;
import tokenizer.Token;
import flike.words.VarAllocWord;

/**
 * Meant to compile variable allocations into something usable.
 * 
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class VarallocWordCompiler implements WordCompilerIF {

    @Override
    public void compile(Token t, FLikeCompiler.CompilerContainer cc) throws CompilerException {
        
        boolean isglob = matchesTokType(cc.tokens[cc.cptr+1], FLikeTokenizer.TokenType.GLOBAL);
        if(isglob) cc.cptr++;

        Token ti = cc.tokens[++cc.cptr];

        if (matchesTokType(ti, FLikeTokenizer.TokenType.IDENTIFIER)) {
            String varname = ti.getSequence();

            debug("Found a varalloc of " + varname);

            cc.compiled.add(new VarAllocWord(varname, isglob ? VarAllocWord.Location.GLOBAL : VarAllocWord.Location.LOCAL));

        } else {
            throw new CompilerException("Varalloc found with an invalid identifier " + ti.getTokenID() + " " + ti.getSequence());
        }
    }

}
