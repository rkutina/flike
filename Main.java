import flike.FLikeExec;
import flike.compiler.FLikeCompiler;
import flike.tokenizer.FLikeTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The Main. Who knew?
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class Main {

    //TODO: turn this into a fairly usable standalone FLike interpreter one day.
    /**
     * The main. It doesn't really do anything useful for now, only used to play
     * around and test some basic functionality. Only contains a few lines to
     * execute a few factorials/fibonacci generators written in FLike for
     * fun.
     *
     * @param args Not actually used as of now.
     */
    public static void main(String[] args) {
        try {
            FLikeTokenizer tknzr = new FLikeTokenizer();
            //tknzr.tokenize("2 if 0 if 3 . else 0 . then 4 . then 2 .");
            //tknzr.tokenize(":: gl 4 . 3 ;; 2 . gl .");
            //tknzr.tokenize("10 0 do I . loop 3 .");

            //tknzr.tokenize("var global A 4.2 A !! A @@ ."); //tests if global var defs work
            tknzr.clear();
            //tknzr.tokenize("var A 4.2 A ! @ .");
            tknzr.tokenize("\"Hello World!\" . .CR \"Vypocet 10 fib cisel:\" . .CR ");
            //tknzr.tokenize("var B 10 B ! begin B -@ dup . -200 = if leave then forever");
            tknzr.tokenize(": fib over over + ; 0 1 10 0 do fib dup . loop dbg");
            //tknzr.tokenize(": fac dup 1 > if dup 1 - fac * then ; 3'5 fac .");

            FLikeCompiler fc = new FLikeCompiler();
            
            fc.compile(tknzr.getTokens());

            FLikeExec fe = new FLikeExec(fc.getGlobals(), null);

            fe.exec(fc.getCompiled(), null);

            System.out.println("\nThat's all.");
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
